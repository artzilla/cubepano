{Native} = require "./native"
{mat4_get} = require "./math"
{min} = Math

DEBUG = off

class WebGL extends Native
  ctx_name = ''
  
  @test: ->
    canvas = document.createElement 'canvas'
    for ctx in [
        'webgl'
        'experimental-webgl'
        'webkit-3d'
        'moz-webgl'
        '3d'
      ]
      try gl = canvas.getContext ctx
      if gl
        ctx_name = ctx
        return yes
    no
  
  onContextLost: (e)=>
    do e.preventDefault
    @active = no
    return

  onContextRestored: =>
    @active = yes
    do @reinit
    do @redraw
    return

  onContextCreationError: (e)=>
    @error = e.statusMessage
    return
  
  init: ->
    @view = @create 'canvas'
    
    @view.addEventListener 'webglcontextlost', @onContextLost, no
    @view.addEventListener 'webglcontextrestored', @onContextRestored, no
    @view.addEventListener 'webglcontextcreationerror', @onContextCreationError, no

    @addChild @view
    
    gl = @gl = @view.getContext ctx_name,
      alpha: no
      depth: no
      stencil: no
      antialias: yes
      preserveDrawingBuffer: yes

    @texOpt =
      limit: gl.getParameter gl.MAX_TEXTURE_SIZE
      format: gl.RGB # setTextureFormat gl
    
    @active = yes
    super
    
    do @reinit
    return

  done: ->
    super
    @delChild @view
    return

  Shader = (gl, gp, type, src) ->
    s = gl.createShader type
    gl.shaderSource s, src
    gl.compileShader s
    if DEBUG
      unless gl.getShaderParameter s, gl.COMPILE_STATUS
        throw new Error (gl.getShaderInfoLog s)
    gl.attachShader gp, s
    return

  Extension = (gl, name) ->
    ext = null
    unless ext = gl.getExtension name
      vendors =
        MOZ: 0
        WEBKIT: 0
      for pfx of vendors
        if ext = gl.getExtension "#{pfx}_#{name}"
          break
    ext

  resizeImage = (image, width, height)->
    can = document.createElement "canvas"
    can.width = width
    can.height = height
    ctx = can.getContext "2d"
    ctx.drawImage image, 0, 0, image.width, image.height, 0, 0, width, height
    can

  fixDimension = (dimension, limit)->
    switch
      when dimension > limit
        limit
      when not /^10*$/.test dimension.toString 2
        1 << round (log dimension) / (log 2)
      else
        dimension

  Texture = (gl, image, opts) ->
    width = fixDimension image.width, opts.limit
    height = fixDimension image.height, opts.limit
    
    if image.width isnt width or image.height isnt height
      image = resizeImage image, width, height
    
    tex = do gl.createTexture
    gl.bindTexture gl.TEXTURE_2D, tex
    gl.pixelStorei gl.UNPACK_FLIP_Y_WEBGL, true
    gl.texImage2D gl.TEXTURE_2D, 0, opts.format, gl.RGB, gl.UNSIGNED_BYTE, image
    
    if gl.NO_ERROR isnt do gl.getError
      # Maybe image is too large
      opts.limit >>= 1
      return Texture gl, image, opts

    # use mipmapping with trilinear filtering to better rendering
    gl.texParameteri gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR
    gl.texParameteri gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR
    gl.generateMipmap gl.TEXTURE_2D
    # use anizotropic filtering extension when available
    tfa = Extension gl, 'EXT_texture_filter_anisotropic'
    if tfa
      gl.texParameterf gl.TEXTURE_2D, tfa.TEXTURE_MAX_ANISOTROPY_EXT,
        min 4, gl.getParameter tfa.MAX_TEXTURE_MAX_ANISOTROPY_EXT
    # setup wrapping to eliminate white glitches between faces
    gl.texParameteri gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE
    gl.texParameteri gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE
    gl.bindTexture gl.TEXTURE_2D, null
    tex

  delTexture = (gl, tex)->
    gl.deleteTexture tex

  reinit: ->
    gl = @gl
    @vertexes = do gl.createBuffer
    @program = program = do gl.createProgram
    
    gl.bindBuffer gl.ARRAY_BUFFER, @vertexes
    gl.bufferData gl.ARRAY_BUFFER, (new Float32Array [
      0.0, 0.0
      0.0, 1.0
      1.0, 1.0
      1.0, 0.0
    ]), gl.STATIC_DRAW
    
    gl.enable gl.CULL_FACE
    gl.frontFace gl.CW
    gl.cullFace gl.BACK
    gl.enable gl.POLYGON_OFFSET_FILL
    gl.polygonOffset 2.0, 10.0
    
    Shader gl, program, gl.VERTEX_SHADER,
      "uniform mat4 m;"+
      "uniform mat4 p;"+
      "attribute vec2 v;"+
      "varying vec2 c;"+
      "void main(){"+
        "c=v;"+
        "gl_Position=p*m*vec4(v-0.5,0.0,1.0);"+
      "}"
    
    Shader gl, program, gl.FRAGMENT_SHADER,
      "precision lowp float;"+
      "uniform lowp sampler2D t;"+
      "varying highp vec2 c;"+
      "void main(){"+
        "gl_FragColor=texture2D(t,c);"+
      "}"
    
    gl.linkProgram program
    
    if DEBUG
      unless gl.getProgramParameter program, gl.LINK_STATUS
        throw new Error gl.getProgramInfoLog program
      gl.validateProgram program
      unless gl.getProgramParameter program, gl.VALIDATE_STATUS
        throw new Error gl.getProgramInfoLog program
    
    do @reload
    do @resize
    do @redraw
    
    return
  
  reconf: ->
    if @textures?.length
      delTexture @gl, tex for tex in @textures when tex?
    @textures = new Array @images.length
    return

  reload: ->
    if @images?
      for image, i in @images
        delTexture @gl, @textures[i] if @textures[i]?
        @textures[i] = if image then Texture @gl, image, @texOpt else null
    return
  
  redraw: ->
    return unless @active and @textures?
    
    gl = @gl
    program = @program
    
    gl.clear gl.COLOR_BUFFER_BIT if DEBUG
    gl.useProgram program
    
    vertexAttr = gl.getAttribLocation program, 'v'
    projMatrix = gl.getUniformLocation program, 'p'
    faceMatrix = gl.getUniformLocation program, 'm'
    textureId = gl.getUniformLocation program, 't'
    
    gl.enableVertexAttribArray vertexAttr
    gl.vertexAttribPointer vertexAttr, 2, gl.FLOAT, false, 0, 0
    
    gl.bindBuffer gl.ARRAY_BUFFER, @vertexes
    
    gl.activeTexture gl.TEXTURE0
    gl.uniform1i textureId, 0
    
    gl.uniformMatrix4fv projMatrix, false, @p8n
    
    for texture, i in @textures when texture?
      gl.uniformMatrix4fv faceMatrix, false, mat4_get @faces, i
      gl.bindTexture gl.TEXTURE_2D, texture
      gl.drawArrays gl.TRIANGLE_FAN, 0, 4
    
    return
  
  resize: ->
    @view.width = @width
    @view.height = @height
    
    @gl.viewport 0, 0, @width, @height
    
    return

WebGL.register 'WebGL'
