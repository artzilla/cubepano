{
  max
  sqrt
} = Math

@UserInput = (Base)->
  lineLength = ({x, y}, p)->
    x -= p.x
    y -= p.y
    sqrt x * x + y * y
  
  lineCenter = (p1, p2)->
    x: (0.5 * (p1.x + p2.x)) | 0
    y: (0.5 * (p1.y + p2.y)) | 0
  
  class UserInput extends Base
    initUI: ->
      @moved = null
      
      if do @isTouch
        @touches = []
        @addEvent "touchstart", @touchStart, @root
      else
        @cursor = null
        @addEvent "mousedown", @mouseDown, @root
        @addEvent "wheel", @mouseWheel, @root
    
      #@addEvent "keydown", @keyDown

    doneUI: ->
      if do @isTouch
        @delEvent "touchstart", @touchStart, @root
        do @touchUnbind
      else
        @delEvent "wheel", @mouseWheel, @root
        @delEvent "mousedown", @mouseDown, @root
        do @mouseUnbind

    handleEvent = (e)->
      do e.preventDefault
      do e.stopPropagation

    mouseWheel: (e)=>
      handleEvent e

      @changeZoom if (e.deltaY or e.detail or e.wheelDelta) > 0 then 1 else -1
      no

    mouseUnbind: ->
      @delEvent "mouseup", @mouseUp
      @delEvent "mousemove", @mouseMove
      @delEvent "mouseover", @mouseOver

    mouseDown: (e)=>
      return unless 1 is @mouseButton e
    
      handleEvent e
    
      @_cursor = @cursor = @mouseCoords e
    
      @addEvent "mouseup", @mouseUp
      @addEvent "mousemove", @mouseMove
      @addEvent "mouseover", @mouseOver
    
      no

    mouseMove: (e)=>
      handleEvent e

      # debounce
      movedTime = do @getTime
      return if @moved? and movedTime - @moved < (max 16, @drawingTime)
      @moved = movedTime

      old_pos = @cursor
      new_pos = @cursor = @mouseCoords e

      @applyMove old_pos, new_pos
      no
  
    mouseUp: (e)=>
      handleEvent e

      do @mouseUnbind
      
      if 3 > lineLength @_cursor, @cursor
        @applyClick e, @cursor
      
      no

    mouseOver: (e)=>
      unless 1 is @mouseButton e
        do @mouseUnbind
        @moved = null

    touchUnbind: ->
      @delEvent "touchend", @touchEnd
      @delEvent "touchcancel", @touchCancel
      @delEvent "touchmove", @touchMove

    touchCoords = ({touches})->
      {x, y} for {pageX: x, pageY: y} in touches

    touchStart: (e)=>
      handleEvent e
    
      if @touches.length is 0
        @addEvent "touchend", @touchEnd
        @addEvent "touchcancel", @touchCancel
        @addEvent "touchmove", @touchMove
    
      @touches = touchCoords e
      no
  
    touchEnd: (e)=>
      handleEvent e
    
      touches = touchCoords e
    
      if touches.length is 0
        do @touchUnbind

        if @moved?
          @moved = null
        else
          if @touches.length is 1
            @applyClick e, @touches[0]

      @touches = touches
      no

    touchCancel: (e)=>
      @touches = []
      @touchEnd touches: []

    touchMove: (e)=>
      handleEvent e
    
      # debounce
      old_touches = @touches
      new_touches = touchCoords e
    
      movedTime = do @getTime
      return if @moved? and movedTime - @moved < (max 16, @drawingTime) and 12 > (lineLength old_touches[0], new_touches[0]) + (if new_touches.length isnt 2 then 0 else lineLength old_touches[1], new_touches[1])
    
      @moved = movedTime
      @touches = new_touches
      
      switch new_touches.length
        when 1
          @applyMove old_touches[0], new_touches[0]
        when 2
          old_dist = lineLength old_touches[0], old_touches[1]
          new_dist = lineLength new_touches[0], new_touches[1]
          @applyZoom (new_dist - old_dist), lineCenter old_touches...
      no

    keyName =
      32: "space"
      13: "enter"
      9: "tab"
      8: "backspace"
      16: "shift"
      17: "ctrl"
      18: "alt"
      20: "capsLock"
      144: "numLock"
      145: "scrollLock"
      37: "left"
      38: "up"
      39: "right"
      40: "down"
      33: "pageUp"

    getKey = (code)->
      keyName[code] or String.fromCharCode code

    onKeyDown: (e)=>
      #getKey e.keyCode

    onKeyUp: (e)=>

@Animation = (Base)->
  class Animation extends Base
    triggerRedraw: (startTime)->
      return if @animation

      startTime ?= do @getTime
      @animation = on

      @requestFrame =>
        endTime = do @getTime
        
        @drawingTime = deltaTime = endTime - startTime
        @prepRedraw deltaTime
  
        if @animation
          @animation = off
          if do @needRedraw
            @triggerRedraw endTime
          else
            @onUpdate? do @getLooking
        
        do @redraw
        do @updatePoints
      
        @onRedraw? deltaTime
