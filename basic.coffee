{max} = Math

class @Basic
  engines = {}
  
  win = window if window?
  doc = win?.document
  
  @register: (name = @name)->
    engines[name] = @

  @list: ->
    defaultEngine = null
    
    for engineName, engineClass of engines when "avail" not in engineClass
      engineClass.avail = do engineClass.test if doc?
      defaultEngine = engineClass if engineClass.avail and not defaultEngine?
    
    for engineName, engineClass of engines
      title: engineName
      avail: engineClass.avail
      bydef: engineClass is defaultEngine
  
  @select: (engineNames)->
    do @list
    
    for engineName, engineClass of engines when not engineNames? or engineNames is engineName or engineNames.length is 0 or engineName in engineNames
      return engineClass if engineClass.avail
    
    null

  constructor: (@root)->
    do @init

  destructor: ->
    do @done

  create: (name, html)->
    node = doc.createElement name
    node.innerHTML = html if html?
    node
  
  delChild: (node)->
    @root.removeChild node
  
  addChild: (node, prepend = no)->
    unless prepend and @root.firstChild?
      @root.appendChild node
    else
      @root.insertBefore node, @root.firstChild

  isTarget: (target, node)->
    while target
      return yes if target is node
      target = target.parentNode
    no

  getOffset: (elm = @root, depth = Infinity)->
    x = 0
    y = 0
    while elm? and depth > 0
      x += elm.offsetLeft
      y += elm.offsetTop
      elm = elm.offsetParent
      depth--
    {x, y}

  getBound: (elm = @root)->
    w = 0
    h = 0
    if elm.getBoundingClientRect?
      {width: w, height: h} = do elm.getBoundingClientRect
    else if elm.outerWidth? and elm.outerHeight?
      w = elm.outerWidth
      h = elm.outerHeight
    else if elm.clientWidth? and elm.clientHeight?
      w = elm.clientWidth
      h = elm.clientHeight
    {w, h}

  applyRange: (value, from, to)->
    value = from if from? and value < from
    value = to if to? and value > to
    value

  eventName: (name)->
    switch name
      when "wheel"
        switch
          when "onwheel" of doc
            name
          when "onmousewheel" of doc
            "mousewheel"
          else
            "MozMousePixelScroll"
      else
        name

  addEvent: switch "function"
    when typeof doc?.addEventListener
      (ev, fn, el = win)-> el.addEventListener (@eventName ev), fn, no
    when typeof doc?.attachEvent
      (ev, fn, el = win)-> el.attachEvent "on#{@eventName ev}", fn
    else
      ->

  delEvent: switch "function"
    when typeof doc?.removeEventListener
      (ev, fn, el = win)-> el.removeEventListener (@eventName ev), fn, no
    when typeof doc?.detachEvent
      (ev, fn, el = win)-> el.detachEvent? "on#{@eventName ev}", fn
    else
      ->

  getTime: switch
    when performance?.now
      -> do performance.now
    when Date.prototype.now
      -> do Date.now
    else
      -> do (new Date).getTime

  if win?
    [requestFrame, cancelFrame] = do ->
      prefixes = [
        ["r", "c"]
        ["webkitR", "webkitC"]
        ["mozR", "mozC"]
        ["msR", "msC"]
        ["oR", "oC"]
      ]
      return [raf, caf] for [r, c] in prefixes when (raf = win["#{r}equestAnimationFrame"])? and (caf = win["#{c}ancelAnimationFrame"] or win["#{c}cancelRequestAnimationFrame"])?
      targetTime = 0
      [
        (cb)->
          targetTime = max targetTime + 16, currentTime = do @getTime
          setTimeout (=> cb targetTime), targetTime - currentTime
        (id)->
          clearTimeout id
      ]

  requestFrame: (onFrame)->
    @frameId = requestFrame onFrame
  
  cancelFrame: ->
    cancelFrame @frameId if @frameId?
    @frameId = null

  mouseCoords: ({pageX, pageY, clientX, clientY})->
    switch
      when pageX?
        x: pageX
        y: pageY
      when clientX?
        html = doc.documentElement
        body = doc.body
        x: clientX + (html.scrollLeft or body and body.scrollLeft or 0) - (html.clientLeft or 0)
        y: clientY + (html.scrollTop or body and body.scrollTop or 0) - (html.clientTop or 0)
      else
        x: 0
        y: 0

  mouseButton: ({which, button})->
    switch
      when which? then which
      when button?
        switch button # do not use a bitwize mask because we need to handle only one button
          when 1 then 1
          when 4 then 2
          when 2 then 3
          else 0
      else 0

  isTouch: ->
    "ontouchstart" of doc.documentElement

  imagesLoad: (urls, cb)->
    i = 0
    c = urls.length
    u = undefined
    m = undefined
  
    next = ->
      if i < c
        u = urls[i]
        i++
        m = new Image
        m.src = u
        if m.complete
          process.nextTick pass
        else
          m.onload = pass
          m.onerror = fail
      return

    pass = ->
      # complete loaded notification
      cb null, i, c, m
      do next
  
    fail = ->
      # loading error notification
      cb (new Error "The image could not be loaded."), i, c, m
      do next

    do next
