{Native} = require "./native"
{
  vec2
  vec2_get
  vec2_set

  vec4
  vec4_num
  vec4_get
  vec4_set
  vec4_transform

  mat4
  mat4_get
  mat4_mul
} = require "./math"

DEBUG = off

class Can2D extends Native
  @test: ->
    try return not not (document.createElement 'canvas').getContext '2d'
    no

  init: ->
    @view = @create 'canvas'
    @ctx = @view.getContext '2d'

    @addChild @view
    
    super
    return

  done: ->
    super
    @delChild @view
    return

  subdiv: 12
  
  reconf: ->
    side = 0.5 + 1e-6
    
    points = @subdiv + 1
    
    @row1 = vec4 points
    @row2 = vec4 points
    
    points *= points
    
    @verts = vec4 points
    @texs = vec2 points
    
    step = side * 2 / @subdiv
    i = 0

    # calc faces verts
    y = -side
    v = 1.0
    
    for r in [0 .. @subdiv]
      x = -side
      u = 0.0
      for c in [0 .. @subdiv]
        # vertex coords
        vec4_set (vec4_get @verts, i), x, y, 0.0, 1.0
        # texture coords
        vec2_set (vec2_get @texs, i), u, v

        i++
        x += step
        u += step

      y += step
      v -= step
    
    return

  reload: ->
    return
  
  renderTriangle: (image, [{xyz: [x0, y0], uv: [u0, v0]}, {xyz: [x1, y1], uv: [u1, v1]}, {xyz: [x2, y2], uv: [u2, v2]}])->
    ctx = @ctx

    do ctx.beginPath
    ctx.moveTo x0, y0
    ctx.lineTo x1, y1
    ctx.lineTo x2, y2
    do ctx.closePath

    u0 *= image.width
    v0 *= image.height
    u1 *= image.width
    v1 *= image.height
    u2 *= image.width
    v2 *= image.height
    
    x1 -= x0
    y1 -= y0
    x2 -= x0
    y2 -= y0
    u1 -= u0
    v1 -= v0
    u2 -= u0
    v2 -= v0

    det = 1.0 / (u1 * v2 - u2 * v1)
    a = (v2 * x1 - v1 * x2) * det
    b = (v2 * y1 - v1 * y2) * det
    c = (u1 * x2 - u2 * x1) * det
    d = (u1 * y2 - u2 * y1) * det
    e = x0 - a * u0 - c * v0
    f = y0 - b * u0 - d * v0

    do ctx.save
    ctx.setTransform a, b, c, d, e, f
    do ctx.clip
    ctx.drawImage image, 0, 0
    do ctx.restore

    return

  testFrustum: (vert)->
    #0 <= vert[0] <= @width and 0 <= vert[1] <= @height and 0 <= vert[2] < 1
    0 <= vert[2] < 1
  
  redraw: ->
    return unless @images?

    if @drawingTime
      if @drawingTime > 32 and @subdiv > 10
        @subdiv -= 1
        do @reconf
      if @drawingTime < 16 and @subdiv < 20
        @subdiv += 1
        do @reconf
    
    ctx = @ctx
    fmat = @tmp_m1
    
    ctx.clearRect 0, 0, @width, @height if DEBUG

    {row1, row2} = @

    if DEBUG
      N = 0
      V = 0

    for image, f in @images when image?
      # apply transform to face
      mat4_mul fmat, @t12n, (mat4_get @faces, f)

      i = 0

      # transform first row of mesh
      for x in [0 .. @subdiv]
        vec4_transform (vert = vec4_get row1, x), fmat, (vec4_get @verts, i++)

      # transform last rows of mesh
      for y in [1 .. @subdiv]
        # transform first vertex of row
        vec4_transform (vert = vec4_get row2, 0), fmat, (vec4_get @verts, i++)
        
        for x in [1 .. @subdiv]
          # transform last vertexes of row
          vec4_transform (vert = vec4_get row2, x), fmat, (vec4_get @verts, i++)
          
          verts = [
            vec4_get row2, x - 1
            vec4_get row2, x
            vec4_get row1, x
            vec4_get row1, x - 1
          ]

          # test vertexes in frustum
          tests = (@testFrustum vert for vert in verts)

          texs = [
            vec2_get @texs, y * (@subdiv + 1) + (x - 1)
            vec2_get @texs, y * (@subdiv + 1) + x
            vec2_get @texs, (y - 1) * (@subdiv + 1) + x
            vec2_get @texs, (y - 1) * (@subdiv + 1) + (x - 1)
          ]

          # generate triangles
          triangles = (for idxs in [[0, 1, 2], [2, 3, 0]] when tests[idxs[0]] and tests[idxs[1]] and tests[idxs[2]]
            for idx in idxs
              xyz: verts[idx]
              uv: texs[idx])

          # render triangles
          for triangle in triangles
            if DEBUG
              V += 3
              N += 1
              
              pal = [
                "black"
                "blue"
                "red"
                "green"
                "purple"
                "teal"
              ]
              do ctx.save
              ctx.lineWidth = 4
              ctx.strokeStyle = pal[f]
              ctx.globalAlpha = 0.2
              do ctx.beginPath
              for {xyz, uv}, k in triangle
                ctx[if k then 'lineTo' else 'moveTo'] xyz[0]|0, xyz[1]|0
              do ctx.closePath
              do ctx.stroke
              do ctx.restore
            
            @renderTriangle image, triangle
            
        [row1, row2] = [row2, row1]

    if DEBUG
      console.log "Amount of quads: #{N} Amount of verts: #{V}"
    
    return
  
  resize: ->
    @view.width = @width
    @view.height = @height

Can2D.register 'Can2D'
