{Basic} = require "./basic"

{
  UserInput
  Animation
} = require "./helper"

{
  vec3
  vec3_get
  vec3_set
  vec3_transform
  
  mat3
  mat3_get
  mat3_set
  mat3_translate
  mat3_scale
  mat3_mul
  mat3_invert
  mat3_viewport2
} = require "./math"

{
  abs
  min
  max
} = Math

epsilon = 0.001

class @FlatPano extends Animation UserInput Basic
  width: 640
  height: 480

  units: "px"
  x: 0
  y: 0
  scale: 1

  xMin: -1
  xMax: 1
  yMin: -1
  yMax: 1
  scaleMin: 1.0
  scaleMax: 1.5
  
  init: ->
    @x = 0
    @y = 0
    @scale = 0

    @targetX = 0
    @targetY = 0
    @targetScale = 0

    @xVel = 1 / 10 / 16
    @yVel = 1 / 10 / 16
    @scaleVel = 0.01

    @xStep = 1
    @yStep = 1
    @scaleStep = 0.01

    v = vec3 6
    @tmp_v1 = vec3_get v, 0
    @tmp_v2 = vec3_get v, 1
    @img_tl = vec3_get v, 2
    @img_br = vec3_get v, 3
    @vpt_tl = vec3_get v, 4
    @vpt_br = vec3_get v, 5

    m = mat3 8
    @camera = mat3_get m, 0 # camera matrix
    @p8n = mat3_get m, 1 # p8n matrix
    @viewport = mat3_get m, 2 # viewport matrix
    @t12n = mat3_get m, 3 # final transformation matrix
    @_t12n = mat3_get m, 4 # inverted transformation matrix
    @model = mat3_get m, 5 # model matrix
    @_model = mat3_get m, 6 # inverted model matrix
    @tmp_m1 = mat3_get m, 7 # static tmp matrix

    do @setViewport
    do @initUI

    @points = []
    @pointNodes = []

  done: ->
    delete @onRedraw
    delete @onUpdate
    delete @onPoint

    do @doneUI

    @setPoints []

  loadImages: (image, cb)->
    cb? null, 0, 1
    @imagesLoad [image], (err, i, c, image)=>
      @reload image
      do @adaptScale
      do @adaptEdges      
      do @triggerRedraw
      cb? err, i, c

  imageStyle: 'display:block;position:absolute;left:0;top:0;'

  reload: (image)->
    @delChild @image if @image?
    
    if image?
      {width, height} = image
      
      @image = image
      @addChild image, yes
      image.setAttribute "style", @imageStyle

      {_model, model, img_tl, img_br} = @

      mat3_viewport2 _model, width, height
      mat3_invert model, _model
      
      vec3_set img_tl, 0, 0, 1
      vec3_transform img_tl, model, img_tl

      vec3_set img_br, width, height, 1
      vec3_transform img_br, model, img_br

      do @recalcPoints

  reproj: ->
    {p8n} = @

    # calculate flat p8n
    mat3_scale p8n, @scale, @scale
    mat3_mul p8n, @camera, p8n
    
    # update t12n matrix
    mat3_mul @t12n, @viewport, p8n
    mat3_invert @_t12n, @t12n

  redraw: ->
    return unless @image?
    
    {t12n, tmp_v1, tmp_v2} = @
    
    vec3_transform tmp_v1, t12n, @img_tl
    vec3_transform tmp_v2, t12n, @img_br

    {style} = @image
    
    style.left = "#{tmp_v1[0]|0}px"
    style.top = "#{tmp_v1[1]|0}px"
    style.width = "#{(tmp_v2[0]-tmp_v1[0])|0}px"
    style.height = "#{(tmp_v2[1]-tmp_v1[1])|0}px"
  
  setViewport: (width, height)->
    @width = width if width?
    @height = height if height?

    if @width > 0 and @height > 0
      {viewport, tmp_m1, vpt_tl, vpt_br} = @
    
      # calculate viewport matrix
      mat3_viewport2 viewport, @width, @height
      mat3_invert tmp_m1, viewport

      vec3_set vpt_tl, 0, 0
      vec3_transform vpt_tl, tmp_m1, vpt_tl

      vec3_set vpt_br, @width, @height
      vec3_transform vpt_br, tmp_m1, vpt_br

      do @adaptScale
      do @adaptEdges
      do @triggerRedraw

  adaptScale: ->
    return unless @image?

    {img_tl, img_br, vpt_tl, vpt_br} = @

    img_w = img_br[0] - img_tl[0]
    img_h = img_tl[1] - img_br[1]

    vpt_w = vpt_br[0] - vpt_tl[0]
    vpt_h = vpt_tl[1] - vpt_br[1]

    scale = max (vpt_w / img_w), (vpt_h / img_h)

    @_scaleMin = scale * @scaleMin
    @_scaleMax = scale * @scaleMax

    @targetScale = @applyRange @targetScale, @_scaleMin, @_scaleMax

  adaptEdges: ->
    return unless @image?
    
    {img_tl, img_br, vpt_tl, vpt_br, tmp_m1, tmp_v1, tmp_v2, targetScale} = @

    mat3_scale tmp_m1, targetScale, targetScale

    vec3_transform tmp_v1, tmp_m1, img_tl
    vec3_transform tmp_v2, tmp_m1, img_br

    @_xMin = -@xMin * (vpt_br[0] - tmp_v2[0])
    @_yMin = -@yMin * (vpt_tl[1] - tmp_v1[1])

    @_xMax = @xMax * (vpt_tl[0] - tmp_v1[0])
    @_yMax = @yMax * (vpt_br[1] - tmp_v2[1])

    @targetX = @applyRange @targetX, @_xMin, @_xMax
    @targetY = @applyRange @targetY, @_yMin, @_yMax

  resize: ->

  setRanges: (ranges)->
    if ranges?
      for par in ['x', 'y', 'scale']
        for key in ["#{par}Min", "#{par}Max"]
          if (val = ranges[key])?
            @[key] = val
      do @setLooking

  setInteract: ({xStep, yStep, scaleStep})->
  
  setLooking: (look)->
    {x, y, scale} = look if look?

    if scale?
      @targetScale = @applyRange scale, @_scaleMin, @_scaleMax
      do @adaptEdges

    if x?
      @targetX = @applyRange x, @_xMin, @_xMax

    if y?
      @targetY = @applyRange y, @_yMin, @_yMax

    do @triggerRedraw

  setLookingRaw: (look)->
    {x, y, scale} = look if look?

    x ?= @x
    y ?= @y
    scale ?= @scale

    @x = @applyRange x, @_xMin, @_xMax
    @y = @applyRange y, @_yMin, @_yMax
    @scale = @applyRange scale, @_scaleMin, @_scaleMax

    # calculate camera location
    mat3_translate @camera, @x, @y

    do @reproj

  needRedraw: ->
    @x isnt @targetX or @y isnt @targetY or @scale isnt @targetScale

  prepRedraw: (deltaTime)->
    {x, y, scale} = @

    x += (@targetX - x) * @xVel * deltaTime
    y += (@targetY - y) * @yVel * deltaTime
    scale += (@targetScale - scale) * @scaleVel * deltaTime

    x = @targetX if epsilon > abs @targetX - x
    y = @targetY if epsilon > abs @targetY - y
    scale = @targetScale if epsilon > abs @targetScale - scale

    @setLookingRaw {x, y, scale}
  
  getLooking: ->
    x: @targetX
    y: @targetY
    scale: @targetScale

  toLooking: (x, y)->
    {tmp_v1} = @
    
    vec3_set tmp_v1, x, y
    vec3_transform tmp_v1, @_t12n, tmp_v1

    [tmp_v1[0], tmp_v1[1]]

  pointElement: 'div'
  pointStyle: 'display:block;position:absolute;left:0;top:0;width:auto;'
  #pointClass: 'panorama-point'
  #pointInnerClass: 'panorama-point-inner'
  #pointOuterClass: 'panorama-point-outer'
  #pointFrontClass: 'panorama-point-front'
  #pointBackClass: 'panorama-point-back'

  setPointClass: (classes)->
    if classes?
      mapping =
        just: 'pointClass'
        inner: 'pointInnerClass'
        outer: 'pointOuterClass'
        back: 'pointBackClass'
        front: 'pointFrontClass'
      
      for classKey, classVal of classes when classKey of mapping
        @[mapping[classKey]] = classVal

      do @updatePoints

  recalcPoints: ->
    return unless @points?.length
    
    for {x, y, pos} in @points
      vec3_set pos, x, y
      vec3_transform pos, @model, pos

    do @updatePoints

  setPoints: (points)->
    @delChild pointNode for pointNode in @pointNodes
    @pointNodes = []

    if points?.length
      poss = vec3 points.length
      
      @points = for {x, y, content}, index in points
        pos = vec3_get poss, index

        pointNode = @create @pointElement, content

        pointNode.setAttribute 'style', @pointStyle
        pointNode.className = @pointClass if @pointClass?

        @pointNodes.push pointNode
        @addChild pointNode

        {w, h} = @getBound pointNode

        hw = (0.5 * w)|0
        hh = (0.5 * h)|0
        
        {style} = pointNode
        style.width = "#{w}px"
        style.height = "#{h}px"
        style.marginLeft = "#{-hw}px"
        style.marginTop = "#{-hh}px"

        {pos, x, y, hw, hh}

      do @recalcPoints
    else
      @points = []

  updatePoints: ->
    return unless @points?.length

    v = @tmp_v1
    
    for {pos, hw, hh}, i in @points
      vec3_transform v, @t12n, pos
      
      out = no
      
      if v[0] < hw
        out = yes
        v[0] = hw

      if v[1] < hh
        out = yes
        v[1] = hh

      if v[0] > @width - hw
        out = yes
        v[0] = @width - hw

      if v[1] > @height - hh
        out = yes
        v[1] = @height - hh

      pointNode = @pointNodes[i]
      {style} = pointNode
      
      style.left = "#{v[0]|0}px"
      style.top = "#{v[1]|0}px"
      
      pointNode.className = (className for className in [
        @pointClass
        if out then @pointOuterClass else @pointInnerClass
      ] when className).join ' '

  handleUpdate: =>
    if @onUpdate?
      @onUpdate
        x: @targetX
        y: @targetY
        scale: @targetScale

  changeZoom: (steps)->
    @setLooking scale: @targetScale - steps * @scaleStep

  applyZoom: (delta, point)->
    {tmp_v1, tmp_v2, _t12n} = @

    vec3_set tmp_v1, 0, 0
    vec3_transform tmp_v1, _t12n, tmp_v1
    
    vec3_set tmp_v2, delta, 0
    vec3_transform tmp_v2, _t12n, tmp_v2

    delta = tmp_v2[0] - tmp_v1[0]
    
    @setLooking scale: @targetScale * (1 + 2 * delta)
  
  applyMove: (old_pos, new_pos)->
    {tmp_v1, tmp_v2, _t12n} = @
    
    vec3_set tmp_v1, old_pos.x, old_pos.y
    vec3_transform tmp_v1, _t12n, tmp_v1

    vec3_set tmp_v2, new_pos.x, new_pos.y
    vec3_transform tmp_v2, _t12n, tmp_v2

    @setLooking
      x: @targetX + (tmp_v2[0] - tmp_v1[0]) * @xStep
      y: @targetY + (tmp_v2[1] - tmp_v1[1]) * @yStep

  applyClick: (e, coord)->
    {target} = e
    root = do @getOffset
    
    {tmp_v1} = @

    vec3_set tmp_v1, (@toLooking coord.x - root.x, coord.y - root.y)...
    vec3_transform tmp_v1, @_model, tmp_v1

    point = null

    if target isnt @view
      for pointNode, pointIndex in @pointNodes when @isTarget target, pointNode
        point = pointIndex
        break
    
    @onPoint? x: tmp_v1[0], y: tmp_v1[1], pointIndex: point
