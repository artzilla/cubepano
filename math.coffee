{min, max, abs, PI, sin, cos, tan, pow} = Math
hPI = PI / 2

r2d = 180.0 / PI
@rad2deg = (x) -> x * r2d

d2r = PI / 180.0
@deg2rad = (x) -> x * d2r

arrayType = Float32Array or Array
arraySlice = arrayType::subarray or arrayType::slice
unless arraySlice?
  arrayInst = new arrayType 1
  arraySlice = arrayInst.subarray or arrayInst.slice

##
# 2 vector utilities
##
@vec2 = (n = 1)->
  new arrayType n * 2

@vec2_num = (v)->
  v.length / 2

@vec2_get = (v, i = 0)->
  b = i * 2
  arraySlice.call v, b, b + 2

@vec2_set = vec2_set = (v, x, y)->
  v[0] = x
  v[1] = y
  return

##
# 3 vector utilities
##

##
# 4 vector utilities
##

@vec3 = vec3 = (n = 1)->
  new arrayType n * 3

@vec3_num = (v)->
  v.length / 3

@vec3_get = (v, i = 0)->
  b = i * 3
  arraySlice.call v, b, b + 3

@vec3_set = vec3_set = (v, x, y, w = 1.0)->
  v[0] = x
  v[1] = y
  v[2] = w
  return

@vec3_std = vec3_std = (r, v)->
  vec3_set r,
    v[0] / v[2],
    v[1] / v[2],
    1.0
  return

@mat3_mul_vec3 = mat3_mul_vec3 = (r, m, v)->
  vec3_set r,
    m[0] * v[0] + m[3] * v[1] + m[6] * v[2],
    m[1] * v[0] + m[4] * v[1] + m[7] * v[2],
    m[2] * v[0] + m[5] * v[1] + m[8] * v[2]

@vec3_transform = (r, m, v)->
  mat3_mul_vec3 r, m, v
  vec3_std r, r
  return

##
# 3x3 matrix utilities
##

@mat3 = (n = 1)->
  new arrayType n * 9

@mat3_num = (m)->
  m.length / 9 | 0

@mat3_get = (m, i = 0)->
  b = i * 9
  arraySlice.call m, b, b + 9

@mat3_set = mat3_set = (m, _11, _21, _31, _12, _22, _32, _13, _23, _33)->
  m[0] = _11
  m[3] = _21
  m[6] = _31
  m[1] = _12
  m[4] = _22
  m[7] = _32
  m[2] = _13
  m[5] = _23
  m[8] = _33
  return

@mat3_identity = (m)->
  mat3_set m,
    1.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 1.0
  return

@mat3_det = mat3_det = (m)->
  m[0] * m[4] * m[8] +
  m[3] * m[7] * m[2] +
  m[6] * m[1] * m[5] -
  m[6] * m[4] * m[2] -
  m[3] * m[1] * m[8] -
  m[0] * m[7] * m[5]

@mat3_invert = mat3_invert = (m, n)->
  det = mat3_det n
  return no if det is 0

  mat3_set m,
    n[4] * n[8] - n[7] * n[5],
    n[6] * n[5] - n[3] * n[8],
    n[3] * n[7] - n[6] * n[4],

    n[7] * n[2] - n[1] * n[8],
    n[0] * n[8] - n[6] * n[2],
    n[6] * n[1] - n[0] * n[7],

    n[1] * n[5] - n[4] * n[2],
    n[3] * n[2] - n[0] * n[5],
    n[0] * n[4] - n[3] * n[1]

  det = 1.0 / det
  m[i] *= det for i in [0... 9]
  yes

@mat3_mul = mat3_mul = (m, b, a)->
  mat3_set m,
    a[0] * b[0] + a[1] * b[3] + a[2] * b[6],
    a[3] * b[0] + a[4] * b[3] + a[5] * b[6],
    a[6] * b[0] + a[7] * b[3] + a[8] * b[6],
    
    a[0] * b[1] + a[1] * b[4] + a[2] * b[7],
    a[3] * b[1] + a[4] * b[4] + a[5] * b[7],
    a[6] * b[1] + a[7] * b[4] + a[8] * b[7],

    a[0] * b[2] + a[1] * b[5] + a[2] * b[8],
    a[3] * b[2] + a[4] * b[5] + a[5] * b[8],
    a[6] * b[2] + a[7] * b[5] + a[8] * b[8]
  return

@mat3_translate = mat3_translate = (m, tx, ty)->
  mat3_set m,
    1.0, 0.0, tx,
    0.0, 1.0, ty,
    0.0, 0.0, 1.0
  return

@mat3_scale = mat3_scale = (m, sx, sy)->
  mat3_set m,
    sx,  0.0, 0.0,
    0.0,  sy, 0.0,
    0.0, 0.0, 1.0
  return

@mat3_rotate = (m, a)->
  ca = cos a
  sa = sin a
  mat3_set m,
    ca,  sa,  0.0,
    -sa, ca,  0.0,
    0.0, 0.0, 1.0
  return

@mat3_viewport = mat3_viewport = (m, x, y, w, h)->
  hw = 0.5 * w
  hh = 0.5 * h
  
  mat3_set m,
    hw,  0, x + hw,
    0, -hh, y + hh,
    0,   0, 1

@mat3_viewport2 = (m, w, h, f = max)->
  s = f w, h
  x = 0.5 * (w - s)
  y = 0.5 * (h - s)
  w = h = s

  mat3_viewport m, x, y, w, h

##
# 4 vector utilities
##

@vec4 = (n = 1)->
  new arrayType n * 4

@vec4_num = (v)->
  v.length / 4

@vec4_get = (v, i = 0)->
  b = i * 4
  arraySlice.call v, b, b + 4

@vec4_set = vec4_set = (v, x, y, z, w = 1.0)->
  v[0] = x
  v[1] = y
  v[2] = z
  v[3] = w
  return

@vec4_std = vec4_std = (r, v)->
  vec4_set r,
    v[0] / v[3],
    v[1] / v[3],
    v[2] / v[3],
    1.0
  return

@mat4_mul_vec4 = mat4_mul_vec4 = (r, m, v)->
  vec4_set r,
    m[0] * v[0] + m[4] * v[1] + m[8] * v[2] + m[12] * v[3],
    m[1] * v[0] + m[5] * v[1] + m[9] * v[2] + m[13] * v[3],
    m[2] * v[0] + m[6] * v[1] + m[10] * v[2] + m[14] * v[3],
    m[3] * v[0] + m[7] * v[1] + m[11] * v[2] + m[15] * v[3]

@vec4_transform = (r, m, v)->
  mat4_mul_vec4 r, m, v
  vec4_std r, r
  return

##
# 4x4 matrix utilities
##

@mat4 = mat4 = (n = 1)->
  new arrayType n * 16

@mat4_num = (m)->
  m.length / 16 | 0

@mat4_get = (m, i = 0)->
  b = i * 16
  arraySlice.call m, b, b + 16

@mat4_set = mat4_set = (m, _11, _21, _31, _41, _12, _22, _32, _42, _13, _23, _33, _43, _14, _24, _34, _44)->
  m[0] = _11
  m[4] = _21
  m[8] = _31
  m[12] = _41
  m[1] = _12
  m[5] = _22
  m[9] = _32
  m[13] = _42
  m[2] = _13
  m[6] = _23
  m[10] = _33
  m[14] = _43
  m[3] = _14
  m[7] = _24
  m[11] = _34
  m[15] = _44
  return

@mat4_identity = mat4_identity = (m)->
  mat4_set m,
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0
  return

@mat4_mul = mat4_mul = (m, b, a)->
  mat4_set m,
    a[0] * b[0] + a[1] * b[4] + a[2] * b[8] + a[3] * b[12],
    a[4] * b[0] + a[5] * b[4] + a[6] * b[8] + a[7] * b[12],
    a[8] * b[0] + a[9] * b[4] + a[10] * b[8] + a[11] * b[12],
    a[12] * b[0] + a[13] * b[4] + a[14] * b[8] + a[15] * b[12],
    
    a[0] * b[1] + a[1] * b[5] + a[2] * b[9] + a[3] * b[13],
    a[4] * b[1] + a[5] * b[5] + a[6] * b[9] + a[7] * b[13],
    a[8] * b[1] + a[9] * b[5] + a[10] * b[9] + a[11] * b[13],
    a[12] * b[1] + a[13] * b[5] + a[14] * b[9] + a[15] * b[13],
    
    a[0] * b[2] + a[1] * b[6] + a[2] * b[10] + a[3] * b[14],
    a[4] * b[2] + a[5] * b[6] + a[6] * b[10] + a[7] * b[14],
    a[8] * b[2] + a[9] * b[6] + a[10] * b[10] + a[11] * b[14],
    a[12] * b[2] + a[13] * b[6] + a[14] * b[10] + a[15] * b[14],
    
    a[0] * b[3] + a[1] * b[7] + a[2] * b[11] + a[3] * b[15],
    a[4] * b[3] + a[5] * b[7] + a[6] * b[11] + a[7] * b[15],
    a[8] * b[3] + a[9] * b[7] + a[10] * b[11] + a[11] * b[15],
    a[12] * b[3] + a[13] * b[7] + a[14] * b[11] + a[15] * b[15]
  return

@mat4_translate = mat4_translate = (m, tx, ty, tz)->
  mat4_set m,
    1.0, 0.0, 0.0, tx,
    0.0, 1.0, 0.0, ty,
    0.0, 0.0, 1.0, tz,
    0.0, 0.0, 0.0, 1.0
  return

@mat4_scale = mat4_scale = (m, sx, sy, sz)->
  mat4_set m,
    sx, 0.0, 0.0, 0.0,
    0.0, sy, 0.0, 0.0,
    0.0, 0.0, sz, 0.0,
    0.0, 0.0, 0.0, 1.0
  return

@mat4_rotate = mat4_rotate = (m, x, y, z, a)->
  ha = 0.5 * a
  sa = sin ha
  sc = sa * cos ha
  sq = sa * sa
  sq2 = 2.0 * sq
  xx = x * x
  yy = y * y
  zz = z * z
  x_sc = x * sc
  y_sc = y * sc
  z_sc = z * sc
  xy_sq = x * y
  yz_sq = y * z
  zx_sq = x * z
  mat4_set m,
    1.0 - ((yy + zz) * sq2), 2.0 * (xy_sq - z_sc),    2.0 * (zx_sq + y_sc),    0.0,
    2.0 * (xy_sq + z_sc),    1.0 - ((xx + zz) * sq2), 2.0 * (yz_sq - x_sc),    0.0,
    2.0 * (zx_sq - y_sc),    2.0 * (yz_sq + x_sc),    1.0 - ((xx + yy) * sq2), 0.0,
    0.0,                     0.0,                     0.0,                     1.0
  return

@mat4_invert = mat4_invert = (m, n)->
  _n = [n[0], n[1], n[2], n[3]]
  
  mat4_set m,
    n[5] * n[10] * n[15] -
    n[5] * n[11] * n[14] -
    n[9] * n[6] * n[15] +
    n[9] * n[7] * n[14] +
    n[13] * n[6] * n[11] -
    n[13] * n[7] * n[10],

    -n[4] * n[10] * n[15] +
    n[4] * n[11] * n[14] +
    n[8] * n[6] * n[15] -
    n[8] * n[7] * n[14] -
    n[12] * n[6] * n[11] +
    n[12] * n[7] * n[10],

    n[4] * n[9] * n[15] -
    n[4] * n[11] * n[13] -
    n[8] * n[5] * n[15] +
    n[8] * n[7] * n[13] +
    n[12] * n[5] * n[11] -
    n[12] * n[7] * n[9],

    -n[4] * n[9] * n[14] +
    n[4] * n[10] * n[13] +
    n[8] * n[5] * n[14] -
    n[8] * n[6] * n[13] -
    n[12] * n[5] * n[10] +
    n[12] * n[6] * n[9],

    -n[1] * n[10] * n[15] +
    n[1] * n[11] * n[14] +
    n[9] * n[2] * n[15] -
    n[9] * n[3] * n[14] -
    n[13] * n[2] * n[11] +
    n[13] * n[3] * n[10],

    n[0] * n[10] * n[15] -
    n[0] * n[11] * n[14] -
    n[8] * n[2] * n[15] +
    n[8] * n[3] * n[14] +
    n[12] * n[2] * n[11] -
    n[12] * n[3] * n[10],

    -n[0] * n[9] * n[15] +
    n[0] * n[11] * n[13] +
    n[8] * n[1] * n[15] -
    n[8] * n[3] * n[13] -
    n[12] * n[1] * n[11] +
    n[12] * n[3] * n[9],

    n[0] * n[9] * n[14] -
    n[0] * n[10] * n[13] -
    n[8] * n[1] * n[14] +
    n[8] * n[2] * n[13] +
    n[12] * n[1] * n[10] -
    n[12] * n[2] * n[9],

    n[1] * n[6] * n[15] -
    n[1] * n[7] * n[14] -
    n[5] * n[2] * n[15] +
    n[5] * n[3] * n[14] +
    n[13] * n[2] * n[7] -
    n[13] * n[3] * n[6],

    -n[0] * n[6] * n[15] +
    n[0] * n[7] * n[14] +
    n[4] * n[2] * n[15] -
    n[4] * n[3] * n[14] -
    n[12] * n[2] * n[7] +
    n[12] * n[3] * n[6],

    n[0] * n[5] * n[15] -
    n[0] * n[7] * n[13] -
    n[4] * n[1] * n[15] +
    n[4] * n[3] * n[13] +
    n[12] * n[1] * n[7] -
    n[12] * n[3] * n[5],

    -n[0] * n[5] * n[14] +
    n[0] * n[6] * n[13] +
    n[4] * n[1] * n[14] -
    n[4] * n[2] * n[13] -
    n[12] * n[1] * n[6] +
    n[12] * n[2] * n[5],

    -n[1] * n[6] * n[11] +
    n[1] * n[7] * n[10] +
    n[5] * n[2] * n[11] -
    n[5] * n[3] * n[10] -
    n[9] * n[2] * n[7] +
    n[9] * n[3] * n[6],

    n[0] * n[6] * n[11] -
    n[0] * n[7] * n[10] -
    n[4] * n[2] * n[11] +
    n[4] * n[3] * n[10] +
    n[8] * n[2] * n[7] -
    n[8] * n[3] * n[6],

    -n[0] * n[5] * n[11] +
    n[0] * n[7] * n[9] +
    n[4] * n[1] * n[11] -
    n[4] * n[3] * n[9] -
    n[8] * n[1] * n[7] +
    n[8] * n[3] * n[5],

    n[0] * n[5] * n[10] -
    n[0] * n[6] * n[9] -
    n[4] * n[1] * n[10] +
    n[4] * n[2] * n[9] +
    n[8] * n[1] * n[6] -
    n[8] * n[2] * n[5]

  det = _n[0] * m[0] + _n[1] * m[4] + _n[2] * m[8] + _n[3] * m[12]
  return no if det is 0
  det = 1.0 / det
  
  m[i] *= det for i in [0... 16]
  yes

@mat4_frustum = mat4_frustum = (m, x, X, y, Y, z, Z)->
  z2 = 2.0 * z
  X_x = X - x
  Y_y = Y - y
  z_Z = z - Z
  mat4_set m,
    z2 / X_x, 0.0,      (X + x) / X_x, 0.0,
    0.0,      z2 / Y_y, (Y + y) / Y_y, 0.0,
    0.0,      0.0,      (Z + z) / z_Z, z2 * Z / z_Z,
    0.0,      0.0,      -1.0,          0.0
  return

@mat4_perspective = (m, fov, aspect, znear, zfar)->
  fov = znear * tan 0.5 * fov
  mat4_frustum m,
    -fov * aspect, fov * aspect,
    -fov,          fov,
    znear,         zfar
  return

@mat4_viewport = (m, x, y, w, h, z, Z)->
  hw = w / 2
  hh = h / 2
  mat4_set m,
    hw, 0,   0,     x + hw,
    0,  -hh, 0,     y + hh,
    0,  0,   Z - z, z,
    0,  0,   0,     1
  return

@mat4_to_css = (m)->
  r = "matrix3d(#{m[0]}"
  r += ",#{m[i]}" for i in [1...16]
  "#{r})"

parse_args = (a)->
  (new Function "PI", "hPI", "return [#{a}];") PI, hPI

check_args = (a, n)->
  throw new Error "Invalid number of args" if n? and a.length != n
  throw new Error "Argument not a number" for x in a when typeof x isnt "number"

tmp_mat4 = do mat4

@mat4_synth = (m, s)->
  mat4_identity m
  s.replace /\s*(r|rotate|t|translate|s|scale)\s*\(([^\)]*)\)/g, (_, op, args)->
    args = parse_args args
    switch op
      when 'r', 'rotate'
        check_args args, 4
        mat4_rotate tmp_mat4, args...
        mat4_mul m, m, tmp_mat4
      when 't', 'translate'
        check_args args, 3
        mat4_translate tmp_mat4, args...
        mat4_mul m, m, tmp_mat4
      when 's', 'scale'
        check_args args, 3
        mat4_scale tmp_mat4, args...
        mat4_mul m, m, tmp_mat4
  return
