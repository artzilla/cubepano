{Basic} = require "./basic"

{
  UserInput
  Animation
} = require "./helper"

{
  vec4
  vec4_get
  vec4_set
  vec4_transform
  
  mat4
  mat4_num
  mat4_get
  mat4_rotate
  mat4_perspective
  mat4_translate
  mat4_scale
  mat4_mul

  mat4_invert
  mat4_viewport
} = require "./math"

{
  max
  sqrt
  
  PI
  
  atan
  atan2

  abs
} = Math

PIx2 = PI * 2
epsilon = 0.001

class @Native extends Animation UserInput Basic
  changeZoom: (steps)->
    @setLooking fov: @targetFov + steps * @fovStep

  applyZoom: (delta, point)->
    @setLooking fov: @targetFov - 2 * @coordToAngle delta
  
  applyMove: (old_pos, new_pos)->
    @setLooking
      lon: @targetLon + @coordToAngle (old_pos.x - new_pos.x) * @latStep
      lat: @targetLat + @coordToAngle (old_pos.y - new_pos.y) * @lonStep

  applyClick: (e, coord)->
    {target} = e
    root = do @getOffset
    
    [lon, lat] = @toLooking coord.x - root.x, coord.y - root.y

    point = null

    if target isnt @view
      for pointNode, pointIndex in @pointNodes when @isTarget target, pointNode
        point = pointIndex
        break
    
    @onPoint? {lon, lat, point}

  near: 0.015
  far: 1.5
  
  init: ->
    @lat = 0
    @lon = 0
    @fov = 120
    
    @targetLat = 0
    @targetLon = 0
    @targetFov = 120
    
    @latVel = 1 / 10 / 16
    @lonVel = 1 / 10 / 16
    @fovVel = 0.01

    @latStep = 1.0
    @lonStep = 1.0
    @fovStep = 0.01

    v = vec4 1
    @tmp_v1 = vec4_get v, 0

    m = mat4 7
    @camera = mat4_get m, 0 # camera matrix
    @p8n = mat4_get m, 1 # projection matrix
    @viewport = mat4_get m, 2 # viewport matrix
    @t12n = mat4_get m, 3 # final transformation matrix
    @_t12n = mat4_get m, 4 # inverse transformation matrix
    @tmp_m1 = mat4_get m, 5 # static tmp_m1 matrix
    @tmp_m2 = mat4_get m, 6

    @drawingTime = 0

    do @initUI
    
    @points = []
    @pointNodes = []

  done: ->
    do @doneUI
    
    @setPoints []

  setRanges: (ranges)->
    for par in ['lon', 'lat', 'fov']
      for key in ["#{par}Min", "#{par}Max"]
        if (val = ranges[key])?
          @[key] = val
    do @setLooking
  
  setGeometry: (faces)->
    @faces = faces
    @images = new Array mat4_num faces
    do @reconf

  setInteract: (lat, lon, fov)->
    @latStep = lat if lat?
    @lonStep = lon if lon?
    @fovStep = fov if fov?
  
  loadImages: (images, cb)->
    cb? null, 0, images.length
    @imagesLoad images, (err, i, c, image)=>
      @images[i - 1] = image
      if i == c
        do @reload
        do @triggerRedraw
      cb? err, i, c

  getLooking: ->
    lon: @targetLon
    lat: @targetLat
    fov: @targetFov

  toLooking: (x, y, d = -1.0)->
    wc = @tmp_v1
    
    vec4_set wc, x, y, d
    vec4_transform wc, @_t12n, wc
    
    [ (-atan2 wc[0], -wc[2]),
      (atan wc[1] / (sqrt wc[0] * wc[0] + wc[2] * wc[2])) ]

  coordToAngle: (c)->
    @targetFov * c / @size

  reproj: ->
    {p8n, t12n} = @

    # calculate perspective projection
    mat4_perspective p8n, (if @width > @height then @fov / @aspect else @fov * @aspect), @aspect, @near, @far
    mat4_mul p8n, p8n, @camera
    
    # update transformation matrix
    mat4_mul t12n, @viewport, p8n
    mat4_invert @_t12n, t12n

  setViewport: (width, height)->
    @width = width
    @height = height
    @aspect = width / height
    @size = max width, height
    
    # calculate viewport matrix
    mat4_viewport @viewport, 0, 0, @width, @height, 0, 1
    
    do @reproj
    do @resize
    do @triggerRedraw

  setLooking: (look)->
    {lon, lat, fov} = look if look?

    if lon?
      lon += PIx2 while lon < -PI
      lon -= PIx2 while lon >= PI
      @targetLon = @applyRange lon, @lonMin, @lonMax

    if lat?
      @targetLat = @applyRange lat, @latMin, @latMax

    if fov?
      @targetFov = @applyRange fov, @fovMin, @fovMax

    do @triggerRedraw
  
  setLookingRaw: (look)->
    {lon, lat, fov} = look if look?
    
    lon ?= @lon
    lat ?= @lat
    fov ?= @fov
    
    lon += PIx2 while lon < -PI
    lon -= PIx2 while lon >= PI

    @lon = @applyRange lon, @lonMin, @lonMax
    @lat = @applyRange lat, @latMin, @latMax
    @fov = @applyRange fov, @fovMin, @fovMax
    
    {camera, tmp_m1} = @

    # calculate camera orientation
    mat4_rotate camera, 0.0, 1.0, 0.0, @lon
    mat4_rotate tmp_m1, 1.0, 0.0, 0.0, @lat
    mat4_mul camera, tmp_m1, camera

    do @reproj

  needRedraw: ->
    @lon isnt @targetLon or @lat isnt @targetLat or @fov isnt @targetFov

  prepRedraw: (deltaTime)->
    {lon, lat, fov} = @

    deltaLon = @targetLon - lon
    deltaLon += PIx2 while deltaLon < -PI
    deltaLon -= PIx2 while deltaLon >= PI
      
    lon += deltaLon * @lonVel * deltaTime
    lat += (@targetLat - lat) * @latVel * deltaTime
    fov += (@targetFov - fov) * @fovVel * deltaTime

    lon = @targetLon if epsilon > abs @targetLon - lon
    lat = @targetLat if epsilon > abs @targetLat - lat
    fov = @targetFov if epsilon > abs @targetFov - fov
      
    @setLookingRaw {lon, lat, fov}

  pointElement: 'div'
  pointStyle: 'display:block;position:absolute;left:0;top:0;width:auto;'
  #pointClass: 'panorama-point'
  #pointInnerClass: 'panorama-point-inner'
  #pointOuterClass: 'panorama-point-outer'
  #pointFrontClass: 'panorama-point-front'
  #pointBackClass: 'panorama-point-back'

  setPoints: (points)->
    @delChild pointNode for pointNode in @pointNodes
    @pointNodes = []

    {tmp_m1, tmp_m2} = @

    dirs = vec4 points.length
    
    @points = for {lon, lat, content}, index in points
      dir = vec4_get dirs, index
      vec4_set dir, 0.0, 0.0, -1.0, 1.0
      
      mat4_rotate tmp_m1, 0.0, 1.0, 0.0, lon
      mat4_rotate tmp_m2, 1.0, 0.0, 0.0, lat
      mat4_mul tmp_m1, tmp_m1, tmp_m2
      
      vec4_transform dir, tmp_m1, dir
      
      pointNode = @create @pointElement, content
      
      pointNode.setAttribute 'style', @pointStyle
      pointNode.className = @pointClass if @pointClass?
      
      @pointNodes.push pointNode
      @addChild pointNode
      
      {w, h} = @getBound pointNode
      
      hw = (0.5 * w)|0
      hh = (0.5 * h)|0
      
      {style} = pointNode
      style.width = "#{w}px"
      style.height = "#{h}px"
      style.marginLeft = "#{-hw}px"
      style.marginTop = "#{-hh}px"

      {dir, hw, hh}

    do @updatePoints

  setPointClass: (classes)->
    mapping =
      just: 'pointClass'
      inner: 'pointInnerClass'
      outer: 'pointOuterClass'
      back: 'pointBackClass'
      front: 'pointFrontClass'
    
    for classKey, classVal of classes when classKey of mapping
      @[mapping[classKey]] = classVal

    do @updatePoints
  
  updatePoints: ->
    return unless @points?.length
    
    v = @tmp_v1
    
    for {dir, hw, hh}, i in @points
      vec4_transform v, @t12n, dir
      
      out = no
      back = no
      
      if v[2] < 0.0
        out = yes

      if v[2] > 1.0
        out = yes
        back = yes
        v[1] = @height - v[1]
      
      if v[0] < hw
        out = yes
        v[0] = hw

      if v[1] < hh
        out = yes
        v[1] = hh

      if v[0] > @width - hw
        out = yes
        v[0] = @width - hw

      if v[1] > @height - hh
        out = yes
        v[1] = @height - hh

      if back
        if v[0] * 2 > @width 
          v[0] = hw
        else
          v[0] = @width - hw

      pointNode = @pointNodes[i]
      {style} = pointNode
      
      style.left = "#{v[0]|0}px"
      style.top = "#{v[1]|0}px"
      
      pointNode.className = (className for className in [
        @pointClass
        if out then @pointOuterClass else @pointInnerClass
        if back then @pointBackClass else @pointFrontClass
      ] when className).join ' '
