{Native} = require "./native"
{
  mat4
  mat4_get
  mat4_mul
  mat4_translate
  mat4_scale
  mat4_to_css
} = require "./math"
{max} = Math

DEBUG = off

class Css3D extends Native
  doc = window?.document
  
  checkStyleProp = (elm, sty, css, val)->
    if sty of elm.style
      elm.style[sty] = val
      val = (getComputedStyle elm).getPropertyValue css
      return val? and val isnt 'none'
    no

  initStyleProp = (sty, val)->
    return null unless getComputedStyle?
    res = null
    elm = doc.createElement 'div'
    css = do (sty.replace /([A-Z])/g, '-$1').toLowerCase
    # Add it to the body to get the computed style
    doc.body.appendChild elm
    if checkStyleProp elm, sty, css, val
      res = {sty, css}
    else
      sty_ = do (sty.charAt 0).toUpperCase + sty.substr 1
      css_ = '-' + css
      vendors =
        webkit: '-webkit-'
        Webkit: '-webkit-'
        O: '-o-'
        MS: '-ms-'
        Moz: '-moz-'
      for pfx, css_pfx of vendors
        if checkStyleProp elm, (sty = pfx + sty_), (css = css_pfx + css_), val
          res = {sty, css}
          break
    doc.body.removeChild elm
    res
  
  transform = null
  transformStyle = null
  transformOrigin = null
  backfaceVisibility = null

  @test: ->
    transform = initStyleProp 'transform', 'matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)'
    transformStyle = initStyleProp 'transformStyle', 'preserve-3d'
    transformOrigin = initStyleProp 'transformOrigin', '50% 50% 0'
    backfaceVisibility = initStyleProp 'backfaceVisibility', 'hidden'
    transform and transformStyle and transformOrigin and (DEBUG or not /Gecko\/\d+/.test navigator.userAgent)

  init: ->
    @view = @create 'div'
    
    @view.setAttribute 'style', 'position:relative;top:0;left:0;width:0;height:0;overflow:hidden'

    @addChild @view

    super
    return

  done: ->
    super
    @delChild @view
    return

  reconf: ->
    @fmat = mat4 @images.length
    
    # remove extra faces
    while @view.children.length > @images.length
      @view.removeChild @view.firstChild

    # add missing faces
    while @view.children.length < @images.length
      face = @create 'div'
      face.setAttribute 'style', "position:absolute;background-repeat:no-repeat;#{transformStyle.css}:preserve-3d;#{transformOrigin.css}:0% 0% 0;#{backfaceVisibility.css}:hidden"
      @view.appendChild face
  
  reload: ->
    if @images?
      temp = @tmp_m1
      
      for {style}, i in @view.children
        {width, height, src} = @images[i]
        face = mat4_get @faces, i
        fmat = mat4_get @fmat, i
        
        style.backgroundImage = "url('#{src}')"
        style.width = "#{width}px"
        style.height = "#{height}px"
        
        scale = 1.001 / max width, height
        
        # invert Y and Z
        mat4_scale temp, scale, -scale, -1.0
        mat4_mul fmat, face, temp
        
        # translate to center
        mat4_translate temp, - width / 2, - height / 2, 0.0
        mat4_mul fmat, fmat, temp
    
    return
  
  redraw: ->
    return unless @fmat?
    pmat = @tmp_m1
    
    for face, i in @view.children
      mat4_mul pmat, @t12n, mat4_get @fmat, i
      face.style[transform.sty] = mat4_to_css pmat
    
    return
  
  resize: ->
    @view.style.width = "#{@width}px"
    @view.style.height = "#{@height}px"
    
    return

Css3D.register 'Css3D'
