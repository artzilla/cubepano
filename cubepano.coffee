{Basic} = require "./basic"
{
  mat4
  mat4_num
  mat4_get
  mat4_synth

  rad2deg
  deg2rad
} = require "./math"

id = (v)-> v

class @CubePano
  compileGeometry = (faces)->
    mats = mat4 faces.length
    for face, i in faces when 'string' is typeof face
      mat4_synth (mat4_get mats, i), face
    mats
  
  faces: compileGeometry [
    't(0, 0, -0.5)' # front
    'r(0, 1, 0,  hPI) t(0, 0, -0.5)' # right
    'r(0, 1, 0,  PI)  t(0, 0, -0.5)' # back
    'r(0, 1, 0, -hPI) t(0, 0, -0.5)' # left
    'r(1, 0, 0,  hPI) t(0, 0, -0.5)' # up
    'r(1, 0, 0, -hPI) t(0, 0, -0.5)' # down
  ]
  
  width: 640
  height: 480
  
  units: "degrees"
  toAngle: deg2rad
  fromAngle: rad2deg

  longitude: 0
  latitude: 0
  viewangle: 100
  
  longitudeMin: -180
  longitudeMax: 180
  latitudeMin: -85
  latitudeMax: 85
  viewangleMin: 20
  viewangleMax: 120

  longitudeStep: 1
  latitudeStep: 1
  viewangleStep: 5
  
  pointerNav: on
  longitudeSpeed: 1
  latitudeSpeed: 1
  viewangleSpeed: 1

  @getList: ->
    do Basic.list
  
  constructor: (@root)->

  destructor: ->
    if @engine?
      @engine.destructor?()
    
      delete @engine.onRedraw
      delete @engine.onUpdate
      delete @engine.onPoint
    
      delete @engine
  
  initEngine: (engines)->
    do @saveLooking
    do @engine?.destructor

    @engines = engines if engines?
    Engine = Basic.select @engines
    @engine = new Engine @root if Engine?
    
    do @setGeometry
    do @setViewport
    do @setRanges
    do @loadImages
    do @setLooking

    @engine.onRedraw = @handleRedraw
    @engine.onUpdate = @handleUpdate

    do @setPointClass
    do @setPoints
    @engine.onPoint = @handlePoint
  
  setUnits: (units)->
    @units = units if units? and units in ["degrees", "radians"]
    switch @units
      when "degrees"
        @toAngle = deg2rad
        @fromAngle = rad2deg
      when "radians"
        @toAngle = @fromAngle = id
  
  setGeometry: (faces)->
    @faces = compileGeometry faces if faces?
    @engine?.setGeometry @faces
  
  setViewport: (width, height)->
    @width = width if width?
    @height = height if height?
    @engine?.setViewport @width, @height
  
  loadImages: (urls, cb)->
    @images = urls if urls?.length is mat4_num @faces
    @engine?.loadImages @images, cb if @images?

  setRanges: (ranges)->
    if ranges?
      for par in ['longitude', 'latitude', 'viewangle']
        for key in ["#{par}Min", "#{par}Max"]
          if (val = ranges[key])?
            @[key] = val

    mapping =
      longitude: 'lon'
      latitude: 'lat'
      viewangle: 'fov'
    
    ranges = {}
    for par, arg of mapping
      for key in ["Min", "Max"]
        ranges["#{arg}#{key}"] = @toAngle @["#{par}#{key}"]

    @engine?.setRanges ranges

  setInteract: (longitudeStep, latitudeStep, viewangleStep)->
    @engine?.setInteract longitudeStep, latitudeStep, @toAngle viewangleStep
  
  setLooking: (looking)->
    if looking?
      for key in ['longitude', 'latitude', 'viewangle']
        if (val = looking[key])?
          @[key] = val

    mapping =
      longitude: 'lon'
      latitude: 'lat'
      viewangle: 'fov'
    
    looking = {}
    for key, arg of mapping
      looking[arg] = @toAngle @[key]
    
    @engine?.setLooking looking
  
  saveLooking: ->
    if @engine?
      mapping =
        longitude: 'lon'
        latitude: 'lat'
        viewangle: 'fov'
      
      looking = do @engine.getLooking
      
      for key, arg of mapping
        @[key] = @fromAngle looking[arg]
  
  getLooking: ->
    do @saveLooking
    {@longitude, @latitude, @viewangle}

  setPointClass: (classes)->
    @pointClass ?= {}
    if classes?
      @pointClass[classKey] = classVal for classKey, classVal of classes
    @engine?.setPointClass @pointClass

  setPoints: (points)->
    @points = points if points?
    if @points?
      newPoints = for point in @points
        newPoint = {}
        for field, value of point
          if field is "longitude" or field is "latitude"
            field = field[0...3]
            value = @toAngle value
          newPoint[field] = value
        newPoint
      @engine?.setPoints newPoints
    else
      @engine?.setPoints []

  handlePoint: ({lon, lat, point})=>
    if @onPoint?
      @onPoint
        longitude: @fromAngle lon
        latitude: @fromAngle lat
        pointIndex: point

  handleUpdate: ({lon, lat, fov})=>
    if @onUpdate?
      @onUpdate
        longitude: @fromAngle lon
        latitude: @fromAngle lat
        viewangle: @fromAngle fov

  handleRedraw: (time)=>
    @onRedraw? time
